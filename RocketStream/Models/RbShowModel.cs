﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketStream.Models
{

    public class RbScheduleModel
    {
        [JsonProperty("schedule")]
        public List<RbShowModel> Schedule;
    }

    public class RbShowModel
    {

        private DateTime timeStart;
        private string show;
        private string title;
        private string youtube;
        private DateTime timeEnd;
        private string topic;
        private long length;
        private string game;
        private string type;
        private string live;
        private long id;


        // Properties

        [JsonProperty("timeStart")]
        public DateTime TimeStart { get; set; }

        [JsonProperty("show")]
        public string Show { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("youtube")]
        public string Youtube { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("topic")]
        public string Topic { get; set; }

        [JsonProperty("length")]
        public long Length { get; set; }

        [JsonProperty("game")]
        public string Game { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("live")]
        public string Live { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

    }

}
