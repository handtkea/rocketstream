﻿namespace RocketStream
{
    partial class FormMainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mBrowserStream = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // mBrowserStream
            // 
            this.mBrowserStream.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mBrowserStream.Location = new System.Drawing.Point(0, 0);
            this.mBrowserStream.MinimumSize = new System.Drawing.Size(20, 20);
            this.mBrowserStream.Name = "mBrowserStream";
            this.mBrowserStream.Size = new System.Drawing.Size(800, 450);
            this.mBrowserStream.TabIndex = 0;
            this.mBrowserStream.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.mBrowserStream_DocumentCompleted);
            // 
            // FormMainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.mBrowserStream);
            this.Name = "FormMainWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser mBrowserStream;
    }
}

