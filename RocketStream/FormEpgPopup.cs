﻿using RocketStream.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RocketStream
{
    public partial class FormEpgPopup : Form
    {

        // Members.
        private System.Windows.Forms.Timer mTimer = new System.Windows.Forms.Timer();

        public FormEpgPopup(RbScheduleModel rbSchedule)
        {
            InitializeComponent();

            // Check if schedule is available.
            if (rbSchedule != null && rbSchedule.Schedule != null && rbSchedule.Schedule.Count > 4)
            {
                lblEpg1TimeStart.Text = rbSchedule.Schedule[0].TimeStart.ToString("HH:mm");
                lblEpg2TimeStart.Text = rbSchedule.Schedule[1].TimeStart.ToString("HH:mm");
                lblEpg3TimeStart.Text = rbSchedule.Schedule[2].TimeStart.ToString("HH:mm");
                lblEpg4TimeStart.Text = rbSchedule.Schedule[3].TimeStart.ToString("HH:mm");
                lblEpg5TimeStart.Text = rbSchedule.Schedule[4].TimeStart.ToString("HH:mm");
                lblEpg1Title.Text = rbSchedule.Schedule[0].Title;
                lblEpg2Title.Text = rbSchedule.Schedule[1].Title;
                lblEpg3Title.Text = rbSchedule.Schedule[2].Title;
                lblEpg4Title.Text = rbSchedule.Schedule[3].Title;
                lblEpg5Title.Text = rbSchedule.Schedule[4].Title;
            }
            else
            {
                lblEpg1TimeStart.Text = "No data available.";
                lblEpg2TimeStart.Text = string.Empty;
                lblEpg3TimeStart.Text = string.Empty;
                lblEpg4TimeStart.Text = string.Empty;
                lblEpg5TimeStart.Text = string.Empty;
                lblEpg1Title.Text = string.Empty;
                lblEpg2Title.Text = string.Empty;
                lblEpg3Title.Text = string.Empty;
                lblEpg4Title.Text = string.Empty;
                lblEpg5Title.Text = string.Empty;
            }

            // Close popup after 5 seconds.
            mTimer.Interval = 5000;
            mTimer.Tick += new EventHandler(TimerTickHandler);
            mTimer.Start();
        }


        // Handles key press events during playback.
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            // Quit appliction.
            if (keyData == Keys.F1)
            {
                this.Close();

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


        // Timer callback.
        private void TimerTickHandler(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
