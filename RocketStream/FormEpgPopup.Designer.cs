﻿namespace RocketStream
{
    partial class FormEpgPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEpgPopup));
            this.lblEpg3Title = new System.Windows.Forms.Label();
            this.lblEpg3TimeStart = new System.Windows.Forms.Label();
            this.lblEpg2Title = new System.Windows.Forms.Label();
            this.lblEpg2TimeStart = new System.Windows.Forms.Label();
            this.lblEpg1Title = new System.Windows.Forms.Label();
            this.lblEpg1TimeStart = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblEpg5Title = new System.Windows.Forms.Label();
            this.lblEpg5TimeStart = new System.Windows.Forms.Label();
            this.lblEpg4Title = new System.Windows.Forms.Label();
            this.lblEpg4TimeStart = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblEpg3Title
            // 
            this.lblEpg3Title.AutoSize = true;
            this.lblEpg3Title.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg3Title.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg3Title.Location = new System.Drawing.Point(206, 87);
            this.lblEpg3Title.Name = "lblEpg3Title";
            this.lblEpg3Title.Size = new System.Drawing.Size(62, 24);
            this.lblEpg3Title.TabIndex = 12;
            this.lblEpg3Title.Text = "Title3";
            // 
            // lblEpg3TimeStart
            // 
            this.lblEpg3TimeStart.AutoSize = true;
            this.lblEpg3TimeStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg3TimeStart.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg3TimeStart.Location = new System.Drawing.Point(119, 87);
            this.lblEpg3TimeStart.Name = "lblEpg3TimeStart";
            this.lblEpg3TimeStart.Size = new System.Drawing.Size(113, 24);
            this.lblEpg3TimeStart.TabIndex = 11;
            this.lblEpg3TimeStart.Text = "TimeStart3";
            // 
            // lblEpg2Title
            // 
            this.lblEpg2Title.AutoSize = true;
            this.lblEpg2Title.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg2Title.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg2Title.Location = new System.Drawing.Point(206, 53);
            this.lblEpg2Title.Name = "lblEpg2Title";
            this.lblEpg2Title.Size = new System.Drawing.Size(62, 24);
            this.lblEpg2Title.TabIndex = 10;
            this.lblEpg2Title.Text = "Title2";
            // 
            // lblEpg2TimeStart
            // 
            this.lblEpg2TimeStart.AutoSize = true;
            this.lblEpg2TimeStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg2TimeStart.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg2TimeStart.Location = new System.Drawing.Point(119, 53);
            this.lblEpg2TimeStart.Name = "lblEpg2TimeStart";
            this.lblEpg2TimeStart.Size = new System.Drawing.Size(113, 24);
            this.lblEpg2TimeStart.TabIndex = 9;
            this.lblEpg2TimeStart.Text = "TimeStart2";
            // 
            // lblEpg1Title
            // 
            this.lblEpg1Title.AutoSize = true;
            this.lblEpg1Title.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg1Title.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg1Title.Location = new System.Drawing.Point(206, 18);
            this.lblEpg1Title.Name = "lblEpg1Title";
            this.lblEpg1Title.Size = new System.Drawing.Size(62, 24);
            this.lblEpg1Title.TabIndex = 8;
            this.lblEpg1Title.Text = "Title1";
            // 
            // lblEpg1TimeStart
            // 
            this.lblEpg1TimeStart.AutoSize = true;
            this.lblEpg1TimeStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg1TimeStart.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg1TimeStart.Location = new System.Drawing.Point(119, 18);
            this.lblEpg1TimeStart.Name = "lblEpg1TimeStart";
            this.lblEpg1TimeStart.Size = new System.Drawing.Size(113, 24);
            this.lblEpg1TimeStart.TabIndex = 7;
            this.lblEpg1TimeStart.Text = "TimeStart1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(85, 93);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // lblEpg5Title
            // 
            this.lblEpg5Title.AutoSize = true;
            this.lblEpg5Title.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg5Title.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg5Title.Location = new System.Drawing.Point(206, 159);
            this.lblEpg5Title.Name = "lblEpg5Title";
            this.lblEpg5Title.Size = new System.Drawing.Size(62, 24);
            this.lblEpg5Title.TabIndex = 17;
            this.lblEpg5Title.Text = "Title5";
            // 
            // lblEpg5TimeStart
            // 
            this.lblEpg5TimeStart.AutoSize = true;
            this.lblEpg5TimeStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg5TimeStart.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg5TimeStart.Location = new System.Drawing.Point(119, 159);
            this.lblEpg5TimeStart.Name = "lblEpg5TimeStart";
            this.lblEpg5TimeStart.Size = new System.Drawing.Size(113, 24);
            this.lblEpg5TimeStart.TabIndex = 16;
            this.lblEpg5TimeStart.Text = "TimeStart5";
            // 
            // lblEpg4Title
            // 
            this.lblEpg4Title.AutoSize = true;
            this.lblEpg4Title.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg4Title.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg4Title.Location = new System.Drawing.Point(206, 122);
            this.lblEpg4Title.Name = "lblEpg4Title";
            this.lblEpg4Title.Size = new System.Drawing.Size(62, 24);
            this.lblEpg4Title.TabIndex = 15;
            this.lblEpg4Title.Text = "Title4";
            // 
            // lblEpg4TimeStart
            // 
            this.lblEpg4TimeStart.AutoSize = true;
            this.lblEpg4TimeStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEpg4TimeStart.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEpg4TimeStart.Location = new System.Drawing.Point(119, 122);
            this.lblEpg4TimeStart.Name = "lblEpg4TimeStart";
            this.lblEpg4TimeStart.Size = new System.Drawing.Size(113, 24);
            this.lblEpg4TimeStart.TabIndex = 14;
            this.lblEpg4TimeStart.Text = "TimeStart4";
            // 
            // FormEpgPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblEpg5Title);
            this.Controls.Add(this.lblEpg5TimeStart);
            this.Controls.Add(this.lblEpg4Title);
            this.Controls.Add(this.lblEpg4TimeStart);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblEpg3Title);
            this.Controls.Add(this.lblEpg3TimeStart);
            this.Controls.Add(this.lblEpg2Title);
            this.Controls.Add(this.lblEpg2TimeStart);
            this.Controls.Add(this.lblEpg1Title);
            this.Controls.Add(this.lblEpg1TimeStart);
            this.Name = "FormEpgPopup";
            this.Text = "FormEpgPopup";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEpg3Title;
        private System.Windows.Forms.Label lblEpg3TimeStart;
        private System.Windows.Forms.Label lblEpg2Title;
        private System.Windows.Forms.Label lblEpg2TimeStart;
        private System.Windows.Forms.Label lblEpg1Title;
        private System.Windows.Forms.Label lblEpg1TimeStart;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblEpg5Title;
        private System.Windows.Forms.Label lblEpg5TimeStart;
        private System.Windows.Forms.Label lblEpg4Title;
        private System.Windows.Forms.Label lblEpg4TimeStart;
    }
}